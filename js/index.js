$(function () {
    $('[data-toggle="popover"]').popover()
  })

$('.carousel').carousel({
    interval: 3000
  });

  $('#staticBackdrop').on('show.bs.modal', function (e) {
    console.log('El modal se está mostrando');
    $('.btn-reserve').removeClass('btn-primary');
    $('.btn-reserve').addClass('btn-secondary');
    $('.btn-reserve').prop('disabled', true);
  });
  $('#staticBackdrop').on('shown.bs.modal', function (e) {
    console.log('El modal se terminó de mostrar');
  });
  $('#staticBackdrop').on('hide.bs.modal', function (e) {
    console.log('El modal se está cerrando');
    $('.btn-reserve').prop('disabled', false);
    $('.btn-reserve').removeClass('btn-secondary');
    $('.btn-reserve').addClass('btn-primary');
  });
  $('#staticBackdrop').on('hidden.bs.modal', function (e) {
    console.log('El modal se terminó de cerrar');
  });